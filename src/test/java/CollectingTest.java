import de.saxsys.javafx.test.JfxRunner;
import javafx.scene.input.KeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import platformer.Main;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.model.Character.Character;
import platformer.model.DropFromSky.Coin.Coin;
import platformer.model.DropFromSky.Item.HeartItem;
import platformer.model.DropFromSky.Item.Item;
import platformer.model.DropFromSky.Item.StarItem;
import platformer.model.DropFromSky.Item.TimeItem;
import platformer.model.DropFromSky.Meteor.Meteor;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JfxRunner.class)
public class CollectingTest {
    private Character onGroundCharacter;
    private ArrayList<Item> items;
    private ArrayList<Meteor> meteors;
    private ArrayList<Coin> coins;

    private DrawingLoop drawingLoopUnderTest;
    private Platform platformUnderTest;
    private Method checkDrawingCollisionMethod;
    private Method paintMethod;

    @Before
    public void setUp() throws NoSuchMethodException, IOException {
        onGroundCharacter = new Character(30, Platform.GROUND - Character.CHARACTER_HEIGHT, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);
        items = new ArrayList<>();
        meteors = new ArrayList<>();
        coins = new ArrayList<>();

        platformUnderTest = new Platform();
        drawingLoopUnderTest = new DrawingLoop(platformUnderTest);
        checkDrawingCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", Character.class, ArrayList.class, ArrayList.class, ArrayList.class);
        checkDrawingCollisionMethod.setAccessible(true);
        paintMethod = DrawingLoop.class.getDeclaredMethod("paint", Character.class, ArrayList.class, ArrayList.class, ArrayList.class);
        paintMethod.setAccessible(true);

        platformUnderTest.setGamePlayRunning(true);

    }

    @Test
    public void whenCharacterCollectedCoinScoreIncrease() throws InvocationTargetException, IllegalAccessException {
        coins.add(new Coin(30, onGroundCharacter.getY() + 10));
        Main.getPlatform().setGamePlayRunning(true);
        int startScore = onGroundCharacter.getScore();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(onGroundCharacter.getScore() > startScore);
    }

    @Test
    public void whenCharacterCollectedMeteorLifeDecrease() throws InvocationTargetException, IllegalAccessException {
        meteors.add(new Meteor(30, onGroundCharacter.getY() - Meteor.METEOR_HEIGHT + 5));
        Main.getPlatform().setGamePlayRunning(true);
        int startLife = onGroundCharacter.getLife();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(2, startLife);
        assertTrue(onGroundCharacter.getLife() < startLife);
    }


    @Test
    public void whenCharacterCollectedStarItemThenScoreIncrease() throws InvocationTargetException, IllegalAccessException {
        items.add(new StarItem(30, onGroundCharacter.getY() - Item.ITEM_HEIGHT + 5));
        Main.getPlatform().setGamePlayRunning(true);
        int startScore = onGroundCharacter.getScore();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(onGroundCharacter.getScore() > startScore);
    }

    @Test
    public void whenCharacterCollectedHeartItemThenLifeIncrease() throws InvocationTargetException, IllegalAccessException {
        items.add(new HeartItem(30, onGroundCharacter.getY() - Item.ITEM_HEIGHT + 5));
        Main.getPlatform().setGamePlayRunning(true);
        int startLife = onGroundCharacter.getLife();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(2, startLife);
        assertTrue(onGroundCharacter.getLife() > startLife);
    }

    @Test
    public void whenCharacterCollectedTimeItemThenTimeIncrease() throws InvocationTargetException, IllegalAccessException {
        items.add(new TimeItem(30, onGroundCharacter.getY() - Item.ITEM_HEIGHT + 5));
        Main.getPlatform().setGamePlayRunning(true);
        int startTime = Main.getPlatform().getTime();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(Main.getPlatform().getTime() > startTime);
    }

    @After
    public void setGamePlayToFalse() {
        Main.getPlatform().setGamePlayRunning(false);
    }
}
