import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CharacterMoveTest.class, CollectingTest.class, BuyCostomeTest.class, CostumeHBoxTest.class, SetBackgroundSoundTest.class, MainMenuLinkTest.class, LanguageTest.class})
public class JunitTestSuite {
}