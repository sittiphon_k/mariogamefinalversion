import org.junit.Before;
import org.junit.Test;

import platformer.controller.PaneController.BuyPaneController;
import platformer.controller.PaneController.SettingPaneController;
import platformer.model.Language;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LanguageTest {

    private Language language;
    private Platform platformSetLanguge;
    private SettingPaneController settingLanguage;
    private BuyPaneController changLanguage;
    private Method refreshPaneLanguageTest;
    private Method initialize;

    @Before
    public void setUp() throws NoSuchMethodException, IOException {

        changLanguage = new BuyPaneController();
        settingLanguage = new SettingPaneController();
        initialize = BuyPaneController.class.getDeclaredMethod("initialize");
        initialize.setAccessible(true);
        refreshPaneLanguageTest =BuyPaneController .class.getDeclaredMethod("refreshPaneLanguage");
        refreshPaneLanguageTest.setAccessible(true);

    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAddNewLanguageWillNotFound() throws InvocationTargetException, IllegalAccessException {
        Language language = Language.valueOf("JP");
        platformSetLanguge.setLanguage(language);
        platformSetLanguge.getLanguage();
        refreshPaneLanguageTest.invoke(settingLanguage);



    }
}