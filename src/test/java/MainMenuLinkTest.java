import de.saxsys.javafx.test.JfxRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import platformer.controller.PaneController.MenuPaneController;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static junit.framework.TestCase.assertEquals;

@RunWith(JfxRunner.class)
public class MainMenuLinkTest {
    private Platform plantformunder;
    private MenuPaneController menuPaneControllerTest;
    private Method openSelectCostumeMethod;
    private Method openShopMethod;
    private Method openSettingMethod;
    private Method existMethod;
    private Method playMethod;

    @Before
    public void setUp() throws IOException, NoSuchMethodException {
        plantformunder = new Platform();
        menuPaneControllerTest = new MenuPaneController();
        playMethod = MenuPaneController.class.getDeclaredMethod("play");
        openSelectCostumeMethod = MenuPaneController.class.getDeclaredMethod("cost");
        openShopMethod = MenuPaneController.class.getDeclaredMethod("shop");
        openSettingMethod = MenuPaneController.class.getDeclaredMethod("setting");
        existMethod = MenuPaneController.class.getDeclaredMethod("exit");
        playMethod.setAccessible(true);
        openShopMethod.setAccessible(true);
        openSelectCostumeMethod.setAccessible(true);
        openSettingMethod.setAccessible(true);
        existMethod.setAccessible(true);
    }

    @Test
    public void MainPageContainingOtherFeaturesWithLinkingPage() throws IOException, InvocationTargetException, IllegalAccessException {
        openShopMethod.invoke(menuPaneControllerTest);
        assertEquals("shop", menuPaneControllerTest.getLinke());
        playMethod.invoke(menuPaneControllerTest);
        assertEquals("play", menuPaneControllerTest.getLinke());
        openSettingMethod.invoke(menuPaneControllerTest);
        assertEquals("setting", menuPaneControllerTest.getLinke());
        openSelectCostumeMethod.invoke(menuPaneControllerTest);
        assertEquals("select", menuPaneControllerTest.getLinke());

    }

}