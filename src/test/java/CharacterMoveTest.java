import de.saxsys.javafx.test.JfxRunner;
import javafx.scene.input.KeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import platformer.Main;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.model.Character.Character;
import platformer.model.Direction;
import platformer.model.DropFromSky.Coin.Coin;
import platformer.model.DropFromSky.Item.Item;
import platformer.model.DropFromSky.Meteor.Meteor;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(JfxRunner.class)
public class CharacterMoveTest {
    private Character onGroundCharacter;
    private ArrayList<Item> items;
    private ArrayList<Meteor> meteors;
    private ArrayList<Coin> coins;

    private DrawingLoop drawingLoopUnderTest;
    private Platform platformUnderTest;
    private GameLoop gameLoopUnderTest;
    private Method updateMethod;
    private Method checkDrawingCollisionMethod;
    private Method paintMethod;

    private KeyCode leftKey = KeyCode.A;
    private KeyCode rightKey = KeyCode.D;
    private KeyCode upKey = KeyCode.W;

    @Before
    public void setUp() throws NoSuchMethodException, IOException {
        onGroundCharacter = new Character(30, Platform.GROUND - Character.CHARACTER_HEIGHT, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);
        items = new ArrayList<>();
        meteors = new ArrayList<>();
        coins = new ArrayList<>();

        platformUnderTest = new Platform();
        gameLoopUnderTest = new GameLoop(platformUnderTest);
        drawingLoopUnderTest = new DrawingLoop(platformUnderTest);
        updateMethod = GameLoop.class.getDeclaredMethod("update", Character.class);
        updateMethod.setAccessible(true);
        checkDrawingCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", Character.class, ArrayList.class, ArrayList.class, ArrayList.class);
        checkDrawingCollisionMethod.setAccessible(true);
        paintMethod = DrawingLoop.class.getDeclaredMethod("paint", Character.class, ArrayList.class, ArrayList.class, ArrayList.class);
        paintMethod.setAccessible(true);

        platformUnderTest.setGamePlayRunning(true);

    }

    @Test
    public void whenPressLeftKeyThanCharacterMoveLeft() throws InvocationTargetException, IllegalAccessException {
        Main.getPlatform().setGamePlayRunning(true);
        int start_x = onGroundCharacter.getX();
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(Direction.NONE, onGroundCharacter.getDirection());
        platformUnderTest.getKeys().add(leftKey);
        assertTrue(platformUnderTest.getKeys().isPressed(leftKey));
        updateMethod.invoke(gameLoopUnderTest, onGroundCharacter);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(Direction.LEFT, onGroundCharacter.getDirection());
        updateMethod.invoke(gameLoopUnderTest, onGroundCharacter);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(onGroundCharacter.getX() < start_x);
    }

    @Test
    public void whenPressRightKeyThanCharacterMoveRight() throws InvocationTargetException, IllegalAccessException {
        Main.getPlatform().setGamePlayRunning(true);
        int start_x = onGroundCharacter.getX();
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(Direction.NONE, onGroundCharacter.getDirection());
        platformUnderTest.getKeys().add(rightKey);
        assertTrue(platformUnderTest.getKeys().isPressed(rightKey));
        updateMethod.invoke(gameLoopUnderTest, onGroundCharacter);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertEquals(Direction.RIGHT, onGroundCharacter.getDirection());
        updateMethod.invoke(gameLoopUnderTest, onGroundCharacter);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(onGroundCharacter.getX() > start_x);
    }

    @Test
    public void whenPressUpKeyThanCharacterJump() throws InvocationTargetException, IllegalAccessException {
        Main.getPlatform().setGamePlayRunning(true);
        int start_y = onGroundCharacter.getY();
        checkDrawingCollisionMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertTrue(onGroundCharacter.isCanJump());
        platformUnderTest.getKeys().add(upKey);
        assertTrue(platformUnderTest.getKeys().isPressed(upKey));
        updateMethod.invoke(gameLoopUnderTest, onGroundCharacter);
        paintMethod.invoke(drawingLoopUnderTest, onGroundCharacter, coins, items, meteors);
        assertFalse(onGroundCharacter.isCanJump());
        assertTrue(onGroundCharacter.getY() < start_y);
    }

    @After
    public void setGamePlayToFalse() {
        Main.getPlatform().setGamePlayRunning(false);
    }
}
