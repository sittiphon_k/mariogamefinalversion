import de.saxsys.javafx.test.JfxRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import platformer.controller.PaneController.BuyPaneController;
import platformer.controller.PaneController.SelectCostumePaneController;
import platformer.model.Costume.CostumeHBox;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

@RunWith(JfxRunner.class)
public class BuyCostomeTest {
    private BuyPaneController buyPaneController;
    private SelectCostumePaneController selectCostumePaneControllerTest;
    private Platform plantformunder;
    private CostumeHBox costume1;
    private CostumeHBox costume2;
    private Method setCostumeClickedMethod;
    private Method buycharacterMethod;

    @Before
    public void setUp() throws IOException, NoSuchMethodException {
        costume1 = new CostumeHBox("Gray and Yellow Coat", 25, "This coat be wear in every situation.", "/img/character/singleCostume/8Syg.png", "8Syg", 170.0, "BOTTOM");
        costume2 = new CostumeHBox("Black and Gray Coat", 30, "This coat be wear in every situation.", "/img/character/singleCostume/1Sgb.png", "1Sgb", 4.0, "TOP");
        selectCostumePaneControllerTest = new SelectCostumePaneController();
        buyPaneController = new BuyPaneController();
        buycharacterMethod = BuyPaneController.class.getDeclaredMethod("buy", Platform.class, CostumeHBox.class);
        buycharacterMethod.setAccessible(true);
        plantformunder = new Platform();
        costume2.setUnlock(false);
        costume1.setUnlock(true);
        setCostumeClickedMethod = SelectCostumePaneController.class.getDeclaredMethod("setCostumeClicked", CostumeHBox.class);
        setCostumeClickedMethod.setAccessible(true);

        plantformunder.setMoney(40);

    }

    @Test
    public void ifMoneyIsEnoughToBuyAlockedCostumeTheMoneyWillReduce() throws InvocationTargetException, IllegalAccessException {
        setCostumeClickedMethod.invoke(selectCostumePaneControllerTest, costume2);
        buycharacterMethod.invoke(buyPaneController, plantformunder, costume2);
        assertEquals(10, plantformunder.getMoney());
        setCostumeClickedMethod.invoke(selectCostumePaneControllerTest, costume1);
        buycharacterMethod.invoke(buyPaneController, plantformunder, costume1);
        plantformunder.setMoney(40);
        assertEquals(40, plantformunder.getMoney());
    }

    @Test
    public void AfterBoughtAlockCostumeWillBeUnlock() throws InvocationTargetException, IllegalAccessException {
        buycharacterMethod.invoke(buyPaneController, plantformunder, costume2);
        assertEquals(true, costume2.isUnlock());
    }
}