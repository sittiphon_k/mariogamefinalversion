import org.junit.Before;
import org.junit.Test;
import platformer.controller.PaneController.BuyPaneController;
import platformer.controller.PaneController.SelectCostumePaneController;
import platformer.model.Costume.CostumeHBox;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class CostumeHBoxTest {
    private CostumeHBox createHboxTest;
    private SelectCostumePaneController selectCostumePaneControllerTest;
    private CostumeHBox costumeBox;
    private BuyPaneController buyPaneController;

    private String imgPath = "/img/character/singleCostume/1Sgb.png";
    private String costumeName = "Black and Gray Coat";
    private int costumePrice = 30;
    private String costumeDetail = "This coat be wear in every situation.";
    private String costumeId = "1Sgb";

    private Method setCostumeClickedMethod;

    @Before
    public void setUp() throws IOException, NoSuchMethodException {
        createHboxTest = new CostumeHBox("Black and Gray Coat", 30, "This coat be wear in every situation.", "/img/character/singleCostume/1Sgb.png", "1Sgb", 4.0, "TOP");

        costumeBox = new CostumeHBox("Black and Gray Coat", 30, "This coat be wear in every situation.", "/img/character/singleCostume/1Sgb.png", "1Sgb", 4.0, "TOP");
        selectCostumePaneControllerTest = new SelectCostumePaneController();
        buyPaneController = new BuyPaneController();
        setCostumeClickedMethod = SelectCostumePaneController.class.getDeclaredMethod("setCostumeClicked", CostumeHBox.class);
        setCostumeClickedMethod.setAccessible(true);
    }

    @Test
    public void whenConstructorIsCalledThenCostumeHBoxFieldsShouldMatchConstructorArguments() {
        assertEquals(costumeName, createHboxTest.getCostumeName());
        assertEquals(costumePrice, createHboxTest.getCostumePrice());
        assertEquals(costumeDetail, createHboxTest.getCostumeDetail());
        assertEquals(imgPath, createHboxTest.getImgPath());
        assertEquals(costumeId, createHboxTest.getCostumeId());
    }

    @Test
    public void whenCostumeBoxIsUnlockThenBoxChangeToNormalColor() throws InvocationTargetException, IllegalAccessException {
        buyPaneController.setBought(costumeBox.isUnlock());
        String style = "-fx-background-color: #E6E6FA ; -fx-background-radius:10; -fx-border-color: #B0C4DE; -fx-border-radius: 10";
        ArrayList<CostumeHBox> box = new ArrayList<>();
        box.add(costumeBox);
        setCostumeClickedMethod.invoke(selectCostumePaneControllerTest, costumeBox);
        assertEquals(false, buyPaneController.isBought());
        assertEquals(false, costumeBox.isUnlock());
        assertEquals(style, selectCostumePaneControllerTest.styleNormal());
    }

}