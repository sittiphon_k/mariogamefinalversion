import de.saxsys.javafx.test.JfxRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import platformer.controller.PaneController.SettingPaneController;
import platformer.view.Platform;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

@RunWith(JfxRunner.class)
public class SetBackgroundSoundTest {
    private Platform plantformunder;
    private SettingPaneController settingPaneController;
    private Method changesoundMethod;

    @Before
    public void setUp() throws IOException, NoSuchMethodException {
        plantformunder = new Platform();
        settingPaneController = new SettingPaneController();
        changesoundMethod = SettingPaneController.class.getDeclaredMethod("changesound", Platform.class);
        changesoundMethod.setAccessible(true);

    }

    @Test
    public void WhenTheProgramStartBackGroundMusicWillBePlayed() throws IOException {
        assertEquals(false, plantformunder.isMuteSound());
    }

    @Test
    public void ClickSettingButtonOnMainPageForUnmuteBackgroundMusic() throws InvocationTargetException, IllegalAccessException {
        plantformunder.setMuteSound(false);
        changesoundMethod.invoke(settingPaneController, plantformunder);
        assertEquals(true, plantformunder.isMuteSound());
        plantformunder.setMuteSound(true);
        changesoundMethod.invoke(settingPaneController, plantformunder);
        assertEquals(false, plantformunder.isMuteSound());

    }

}