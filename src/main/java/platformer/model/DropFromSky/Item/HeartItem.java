package platformer.model.DropFromSky.Item;

public class HeartItem extends Item {
    public HeartItem() {
        super("/img/item/Heart.png", "Heart", ItemType.LIFE);
    }

    public HeartItem(int x, int y) {
        super(x, y, "/img/item/Heart.png", "Heart", ItemType.LIFE);
    }

    @Override
    public int getStat() {
        return 1;
    }
}