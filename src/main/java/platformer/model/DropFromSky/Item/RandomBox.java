package platformer.model.DropFromSky.Item;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import platformer.view.Platform;

import java.util.Random;

public class RandomBox extends Pane {
    private int x;
    private int y;

    private final int BOX_WIDTH = 50;
    private final int BOX_HEIGHT = 50;

    private ImageView imageView;

    public RandomBox(int x, int y) {
        this.x = x;
        this.y = y;
        setTranslateX(x);
        setTranslateY(y);
        Image boxImage = new Image(getClass().getResourceAsStream("/img/item/randomBox.png"));
        this.imageView = new ImageView(boxImage);
        this.imageView.setFitWidth(BOX_WIDTH);
        this.imageView.setFitHeight(BOX_HEIGHT);
        this.setVisible(false);
        this.getChildren().addAll(imageView);
    }

    public void randomAppear() {
        this.x = randomX();
        this.y = randomY();
        setTranslateX(x);
        setTranslateY(y);
        this.setVisible(true);
    }

    public int getStat() {
        int ran = new Random().nextInt(3) + 1;
        int stat = 0;
        if (ran == 1) {

        } else if (ran == 2) {

        } else if (ran == 3) {

        }
        return stat;
    }

    public void animateFlowItemUp(int index) {

    }

    public void hide() {
        this.x = 0;
        this.y = -80;
        setTranslateX(x);
        setTranslateY(y);
        this.setVisible(false);
    }

    public int randomX() {
        return new Random().nextInt(Platform.WIDTH - BOX_WIDTH) + 20;
    }

    public int randomY() {
        return new Random().nextInt(80) + 60;
    }
}