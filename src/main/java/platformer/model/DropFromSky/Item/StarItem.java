package platformer.model.DropFromSky.Item;

import java.util.Random;

public class StarItem extends Item {
    public StarItem() {
        super("/img/item/Star.png", "Star", ItemType.MONEY);
    }

    public StarItem(int x, int y) {
        super(x, y, "/img/item/Star.png", "Star", ItemType.MONEY);
    }

    @Override
    public int getStat() {
        return new Random().nextInt(15) + 5;
    }
}