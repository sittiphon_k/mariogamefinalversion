package platformer.model.UI_Element;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class LifeBar extends Pane {
    private int life;
    private Label lifeScore;
    private Image heart;
    private ImageView heartImg;

    public LifeBar(int x, int y) {
        this.life = 0;

        heart = new Image(getClass().getResourceAsStream("/img/icon/heart01.png"));
        heartImg = new ImageView();
        heartImg.setImage(heart);
        heartImg.setFitHeight(40);
        heartImg.setFitWidth(40);

        lifeScore = new Label(Integer.toString(life));
        lifeScore.setFont(Font.font("Verdana", FontWeight.BOLD, 30));
        lifeScore.setTextFill(Color.web("#FFF"));
        lifeScore.setLayoutX(50);
        lifeScore.setLayoutY(3);

        setTranslateX(x);
        setTranslateY(y);
        getChildren().addAll(heartImg, lifeScore);
    }

    public void decreaseLifeBy(int life) {
        this.life = this.life - life;
        setLifeScore(this.life);
    }

    public void increaseLifeBy(int life) {
        this.life = this.life + life;
        setLifeScore(this.life);
    }

    public void setLifeScore(int x) {
        this.lifeScore.setText(Integer.toString(x));
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

}