package platformer.model;

public enum Direction {
    LEFT, RIGHT, NONE
}
