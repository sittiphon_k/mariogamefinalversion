package platformer.controller;

import platformer.model.DropFromSky.Coin.Coin;
import platformer.model.DropFromSky.Meteor.Meteor;
import platformer.view.Platform;

import java.util.ArrayList;

public class AnimationLoop implements Runnable {
    private Platform platform;
    private ArrayList<Coin> coins;
    private ArrayList<Meteor> meteors;
    private boolean running;
    private float interval = 1000.0f / 60;

    public AnimationLoop(Platform platform) {
        this.platform = platform;
        this.coins = platform.getCoinList();
        this.meteors = platform.getMeteors();
        running = true;
    }

    public void coinTick() {
        for (Coin coin : coins) {
            coin.getImageView().tick();
        }
    }

    public void meteorTick() {
        for (Meteor meteor : meteors) {
            meteor.getImageView().tick();
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) interval);
            } catch (InterruptedException e) {
            }
            if (platform.isGamePlayRunning()) {
                try {
                    Thread.sleep((long) (30 - interval));
                } catch (InterruptedException e) {
                }
                coinTick();
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                }
                meteorTick();
                coinTick();
            }
        }
    }
}