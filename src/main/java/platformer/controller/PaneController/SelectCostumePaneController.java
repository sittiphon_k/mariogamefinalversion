package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import platformer.Main;
import platformer.model.Costume.CostumeHBox;
import platformer.model.Language;

import java.io.IOException;
import java.util.ArrayList;

public class SelectCostumePaneController {
    private ArrayList<CostumeHBox> costumeList;
    private String selectedCostume;

    @FXML
    private Label titleLabel;

    @FXML
    private Button closeBtn;

    @FXML
    private ImageView background;

    @FXML
    private AnchorPane showCostume;

    @FXML
    public AnchorPane allCostumePane;

    @FXML
    private Button selectBtn;

    @FXML
    private Label moneyLabel;

    @FXML
    public void initialize() {
        costumeList = BuildCostumeCollection.getAllContume();
        checkCostumeUnlock();

        setStyleLockAllCoustume();
        for (CostumeHBox costumeHBox : costumeList) {
            setCostumeClicked(costumeHBox);
        }

        allCostumePane.getChildren().addAll(costumeList);

        moneyLabel.setText(Integer.toString(Main.getPlatform().getMoney()));
        selectBtn.setDisable(true);

        closeBtn.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        background.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        selectBtn.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().changeCostume(selectedCostume);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        refreshPaneLanguage();
    }

    public void setCostumeClicked(CostumeHBox box) {
        if (box.isUnlock()) {
            box.setStyle(styleNormal());
            box.setOnMouseClicked(event -> {
                otherCostumeBackToNormal();
                selectedCostume = box.getCostumeId();
                setShowImg(box.getImgPath());
                box.setStyle(styleMousePressed());
                selectBtn.setDisable(false);
            });
        } else {
            box.setOnMouseClicked(event -> {
                otherCostumeBackToNormal();
                setShowImg(box.getImgPath());
                box.setStyle(styleLockSellected());
                selectBtn.setDisable(true);
            });
        }
    }

    public void otherCostumeBackToNormal() {
        setStyleLockAllCoustume();
        for (String platformCosId : Main.getPlatform().getUnlockCostumeList()) {
            for (CostumeHBox costumeHBox : costumeList) {
                if (platformCosId.equals(costumeHBox.getCostumeId())) {
                    costumeHBox.setStyle(styleNormal());
                }
            }
        }
    }

    public void setStyleLockAllCoustume() {
        for (CostumeHBox costumeHBox : costumeList) {
            costumeHBox.setStyle(styleLock());
        }
    }

    public String styleNormal() {
        return "-fx-background-color: #E6E6FA ; -fx-background-radius:10; -fx-border-color: #B0C4DE; -fx-border-radius: 10";
    }

    public String styleLock() {
        return "-fx-background-color: #ff595f; -fx-background-radius:10;";
    }

    public String styleLockSellected() {
        return "-fx-background-color: #ba474c; -fx-background-radius:10;";
    }

    public String styleMousePressed() {
        return "-fx-background-color: #B0C4DE; -fx-background-radius:10;";
    }

    public void checkCostumeUnlock() {
        for (CostumeHBox costume : costumeList) {
            for (String unlockCostume : Main.getPlatform().getUnlockCostumeList()) {
                if (costume.getCostumeId().equals(unlockCostume)) {
                    costume.setUnlock(true);
                }
            }
        }
    }

    public void setShowImg(String imgPath) {
        showCostume.getChildren().clear();

        Image img = new Image(getClass().getResourceAsStream(imgPath));
        ImageView imgShowCostume = new ImageView(img);
        imgShowCostume.setFitHeight(150);
        imgShowCostume.setFitWidth(150);
        AnchorPane.setLeftAnchor(imgShowCostume, 15.0);
        AnchorPane.setRightAnchor(imgShowCostume, 15.0);
        AnchorPane.setTopAnchor(imgShowCostume, 50.0);
        AnchorPane.setBottomAnchor(imgShowCostume, 20.0);
        showCostume.getChildren().add(imgShowCostume);
    }

    public void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            selectBtn.setText("Select");
            titleLabel.setText("COSTUME");
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            selectBtn.setText("เลือก");
            titleLabel.setText("ชุดตัวละคร");
        } else {
            selectBtn.setText("选择");
            titleLabel.setText("服装");
        }
    }
}