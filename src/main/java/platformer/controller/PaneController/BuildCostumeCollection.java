package platformer.controller.PaneController;

import platformer.model.Costume.CostumeHBox;

import java.util.ArrayList;

public class BuildCostumeCollection {
    public static ArrayList<CostumeHBox> getAllContume() {
        ArrayList<CostumeHBox> costumeList = new ArrayList<>();
        costumeList.add(new CostumeHBox("Black and Gray Coat", 30, "This coat be wear in every situation.", "/img/character/singleCostume/1Sgb.png", "1Sgb", 4.0, "TOP"));
        costumeList.add(new CostumeHBox("Red and Orange Coat", 25, "This coat be wear in every situation.", "/img/character/singleCostume/2Syr.png", "2Syr", 87.0, "TOP"));
        costumeList.add(new CostumeHBox("Light Blue and Red Coat", 25, "This coat be wear in every situation.", "/img/character/singleCostume/3Sbrb.png", "3Sbrb", 170.0, "TOP"));
        costumeList.add(new CostumeHBox("Yellow and Red Coat", 35, "This coat be wear in every situation.", "/img/character/singleCostume/4Srw.png", "4Srw", 253.0, "TOP"));
        costumeList.add(new CostumeHBox("Dark Gray and Gray Coat", 15, "This coat be wear in every situation.", "/img/character/singleCostume/5Sgg.png", "5Sgg", 336.0, "TOP"));
        costumeList.add(new CostumeHBox("White and Green Coat", 30, "This coat be wear in every situation.", "/img/character/singleCostume/6Sgrw.png", "6Sgrw", 4.0, "BOTTOM"));
        costumeList.add(new CostumeHBox("Black and Brown Coat", 40, "This coat be wear in every situation.", "/img/character/singleCostume/7Sbrb.png", "7Sbrb", 87.0, "BOTTOM"));
        costumeList.add(new CostumeHBox("Gray and Yellow Coat", 25, "This coat be wear in every situation.", "/img/character/singleCostume/8Syg.png", "8Syg", 170.0, "BOTTOM"));
        costumeList.add(new CostumeHBox("Blue Coat", 20, "This coat be wear in every situation.", "/img/character/singleCostume/9Sbb.png", "9Sbb", 253.0, "BOTTOM"));
        costumeList.add(new CostumeHBox("Green and Yellow Coat", 35, "This coat be wear in every situation.", "/img/character/singleCostume/10Sygr.png", "10Sygr", 336.0, "BOTTOM"));
        return costumeList;
    }
}