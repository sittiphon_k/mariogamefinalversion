package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import platformer.Main;
import platformer.model.Language;
import platformer.view.Platform;

import java.io.IOException;
import java.util.ArrayList;

public class SettingPaneController {
    private static ImageView languageBtnStatic;
    private static Label soundLabelStatic;
    private static Label langLabelStatic;
    private static ArrayList<Image> flagImg;

    @FXML
    private ImageView backBtn;

    @FXML
    private ImageView soundBtn;

    @FXML
    private Label soundLabel;

    @FXML
    private ImageView languageBtn;

    @FXML
    private Label languageLabel;

    @FXML
    private void initialize() {
        flagImg = new ArrayList<>();
        flagImg.add(new Image(getClass().getResourceAsStream("/img/icon/USA_flag.png")));
        flagImg.add(new Image(getClass().getResourceAsStream("/img/icon/TH_flag.png")));
        flagImg.add(new Image(getClass().getResourceAsStream("/img/icon/CH_flag.png")));
        languageBtnStatic = languageBtn;
        soundLabelStatic = soundLabel;
        langLabelStatic = languageLabel;

        setIconSound();
        refreshPaneLanguage();

        backBtn.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        soundBtn.setOnMouseClicked(event -> {
            Main.getPlatform().setMuteSound(!Main.getPlatform().isMuteSound());
            setIconSound();
        });

        languageBtn.setOnMouseClicked(event -> {
            Pane selectPane = null;
            try {
                selectPane = FXMLLoader.load(getClass().getResource("/fxml/ChoseLanguagePane.fxml"));
            } catch (IOException e) {
            }
            Main.getPlatform().getChildren().addAll(selectPane);
        });

        refreshPaneLanguage();
    }

    public void setIconSound() {
        Image image;

        if (Main.getPlatform().isMuteSound()) {
            image = new Image(getClass().getResourceAsStream("/img/icon/mute.png"));
        } else {
            image = new Image(getClass().getResourceAsStream("/img/icon/unmute.png"));
        }
        soundBtn.setImage(image);
    }

    public void changesound(Platform platform) {
        platform.setMuteSound(!platform.isMuteSound());
    }

    public static void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            languageBtnStatic.setImage(flagImg.get(0));
            soundLabelStatic.setText("SOUND");
            langLabelStatic.setText("ENGLISH");
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            languageBtnStatic.setImage(flagImg.get(1));
            soundLabelStatic.setText("เสียง");
            langLabelStatic.setText("ภาษาไทย");
        } else {
            languageBtnStatic.setImage(flagImg.get(2));
            soundLabelStatic.setText("声音");
            langLabelStatic.setText("中文");
        }
    }
}