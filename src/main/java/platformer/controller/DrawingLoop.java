package platformer.controller;

import platformer.model.Character.Character;
import platformer.model.DropFromSky.Coin.Coin;
import platformer.model.DropFromSky.Item.Item;
import platformer.model.DropFromSky.Meteor.Meteor;
import platformer.view.Platform;

import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(Character character, ArrayList<Coin> coinList, ArrayList<Item> itemList, ArrayList<Meteor> meteorList) throws InterruptedException {
        character.checkReachGameWall();
        character.checkReachHighest();
        character.checkReachFloor();
        for (Coin coin : coinList) {
            coin.checkReachFloor();
            if (character.getBoundsInParent().intersects(coin.getBoundsInParent())) {
                character.collectedCoin(coin);
            }
        }
        for (Item item : itemList) {
            item.checkReachFloor();
            if (character.getBoundsInParent().intersects(item.getBoundsInParent())) {
                character.itemOver(item);
            }
        }
        for (Meteor meteor : meteorList) {
            meteor.checkReachFloor();
            if (character.getBoundsInParent().intersects(meteor.getBoundsInParent())) {
                character.meteorOvered(meteor);
            }
        }
    }

    private void paint(Character character, ArrayList<Coin> coinList, ArrayList<Item> itemList, ArrayList<Meteor> meteorList) {
        character.repaint();
        for (Coin coin : coinList) {
            coin.repaint();
        }
        for (Meteor meteor : meteorList) {
            meteor.repaint();
        }
        for (Item item : itemList) {
            item.repaint();
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) interval);
            } catch (InterruptedException e) {
            }
            if (platform.isGamePlayRunning()) {
                try {
                    checkDrawCollisions(platform.getCharacter(), platform.getCoinList(), platform.getItems(), platform.getMeteors());
                } catch (InterruptedException e) {
                }
                paint(platform.getCharacter(), platform.getCoinList(), platform.getItems(), platform.getMeteors());
            }
        }
    }
}
