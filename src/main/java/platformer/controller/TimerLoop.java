package platformer.controller;

import platformer.view.Platform;

public class TimerLoop implements Runnable {
    private Platform platform;
    private boolean running;
    private int count = 0;
    private float interval = 1000.0f / 60;

    public TimerLoop(Platform platform) {
        this.platform = platform;
        this.running = true;
    }

    public void updateTime() {
        javafx.application.Platform.runLater(() -> {
            try {
                platform.reduceTime();
            } catch (InterruptedException e) {
            }
        });
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) interval);
            } catch (InterruptedException e) {
            }
            if (platform.isGamePlayRunning()) {
                try {
                    Thread.sleep((long) (1000 - interval));
                } catch (InterruptedException e) {
                }
                updateTime();
                if (count == 5) {
                    platform.fallSomeItem();
                    count = 0;
                }
                count++;
            }
        }
    }

    public void setCount(int count) {
        this.count = count;
    }
}